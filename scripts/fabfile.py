from fabric.colors import blue, green, yellow, red
from fabric.api import * #@UnusedWildImport pylint: disable=W0401
import inspect

env.site_dir = '/opt/atlassian'
env.shell = "/bin/bash -l -i -c" 
env.use_ssh_config = True

demo_hosts = (
('14.38', '4028', 'Roger Barnes'),
('14.199', '4036', 'Roger Barnes'),
('15.122', '4020', 'Roger Barnes'),
('15.204', '4011', 'Roger Barnes'),
('12.81', '4046', 'John Masson'),
('13.238', '4025', 'John Masson'),
('15.62', '4014', 'John Masson'),
('15.1', '4012', 'John Masson'),
('13.186', '4002', 'Dan Radigan'),
('15.45', '4004', 'Dan Radigan'),
('14.66', '4018', 'Dan Radigan'),
('14.238', '4045', 'Dan Radigan'),
('14.41', '4031', 'Jens Schumacher'),
('12.95', '4024', 'Jens Schumacher'),
('14.72', '4026', 'Jens Schumacher'),
('15.254', '4023', 'Jens Schumacher'),
)

# -----------------------------------------------------------------------------
# Environments
# -----------------------------------------------------------------------------
def show_hosts():
    all()
    print ' '.join(env.hosts)

def one():
    env.hosts = ['atlassian@172.28.%s' % h[0] for h in demo_hosts][:1]

def all():
    env.hosts = ['atlassian@172.28.%s' % h[0] for h in demo_hosts]

def host_type():
    run('uname -s')

def bootstrap(clone_opts='--depth=1 --no-single-branch'):
    sudo('install -m 0755 -o $USER -d /opt')
    with cd('/opt'):
        run('git clone https://bitbucket.org/atlassian/teams-in-space-demo.git %s atlassian' % clone_opts)
    with cd(env.site_dir):
        run('git checkout master')
        tis('install')

def tis(command, *args, **kwargs):
    _tis_run('tis-%s' %command, *args, **kwargs)

def put_jdk():
    put('/opt/jdk-7u67-macosx-x64.dmg', 'Desktop/')

def old_put_tis():
    run('mkdir -p /opt/atlassian')
    put('/opt/allthethings/*', '/opt/atlassian')

def put_tis():
    from fabric.contrib.project import rsync_project
    rsync_project('/opt/atlassian', '/opt/allthethings/', delete=True, extra_opts='-P')
    
def put_keys():
    put('id_rsa*', '/Users/atlassian/.ssh/')

def put_chrome():
    put('googlechrome.dmg', '/tmp/')

def put_hipchat():
    put('HipChat-2.6-98.zip', '/tmp/')

def put_bash_profile():
    put('bash_profile', '/Users/atlassian/.bash_profile')

# -----------------------------------------------------------------------------
# Functions not to be run directly
# -----------------------------------------------------------------------------

def _fn():
    """
    Returns current function name
    """
    return inspect.stack()[1][3]

def _tis_run(command, func=run, base_dir=None, *args, **kwargs):
    base_dir = base_dir or env.site_dir
    with cd(base_dir):
        with prefix("source ./setup.sh"):
            return func(command, *args, **kwargs)

