<?php

/*
This function must
make sure the file has been downloaded into the correct directory structure
and return the file location.
*/
function downloadProduct($name, $url, $dir, $filename)
{
    $targetDir = join_paths($dir, strtolower($name));
    if (!file_exists($targetDir)) {
        if (!mkdir($targetDir, 0774, true)) {
            die('Cannot create download cache in ' . $targetDir);
        }
    }
    printAndUnderline("Downloading " . $name);
    $targetFile = join_paths($targetDir, $filename);
    if (!file_exists($targetFile)) {
        runCommand("curl -L " . $url . " -o " . $targetFile . ".$$ && mv ". $targetFile . ".$$ ". $targetFile);
    } else {
        echo("File already exists. Nothing to download." . PHP_EOL);
    }
    return $targetFile;
}

/*
This function must
make sure the file has been downloaded into the correct directory structure
and return the file location.
*/
function downloadPlugin($name, $url, $dir, $product)
{
    $targetDir = join_paths($dir, strtolower($product), $name);
    if (!file_exists($targetDir)) {
        if (!mkdir($targetDir, 0774, true)) {
            die('Cannot create download cache in ' . $targetDir);
        }
    }
    printAndUnderline("Downloading " . $name);
    $targetDirContents = array_diff(scandir($targetDir), array('..', '.'));
    if (sizeof($targetDirContents) == 0) {
        $cwd = getcwd();
        chdir($targetDir);
        runCommand("curl -O -J -L " . $url);
        chdir($cwd);
        $targetDirContents = array_diff(scandir($targetDir), array('..', '.'));
    } else {
        echo("File already exists. Nothing to download." . PHP_EOL);
    }
    return join_paths($targetDir, reset($targetDirContents));
}

/*
This function must
make sure the product has been expanded (either tar.gz or zip)
and return the product's expanded location.
*/
function expandProduct($productName, $downloadedFile, $dir, $productArchiveFile, $productDirName)
{
    if (!file_exists($dir)) {
        if (!mkdir($dir, 0774, true)) {
            die('Cannot create expand cache in ' . $dir);
        }
    }
    $cwd = getcwd();
    $archiveFile = join_paths($dir, $productArchiveFile);
    printAndUnderline("Expanding " . $productName);
    runCommand("cp " . $downloadedFile . " " . $archiveFile);
    $targetDir = str_replace(array(".zip", ".tar.gz"), "", $archiveFile);
    if (file_exists($targetDir)) {
        runCommand("rm -Rf " . $targetDir);
    }
    chdir($dir);
    if (endsWith($productArchiveFile, ".tar.gz")) {
        $command = "tar -zxf " . $archiveFile;
    } else {
        $command = "unzip -q " . $archiveFile . ' -x "__MACOSX*"';
    }
    runCommand($command);
    runCommand("rm " . $archiveFile);
    chdir($cwd);
    return join_paths($dir, $productDirName);
}

/*
This function must
make sure the plugin has been expanded (either jar or obr)
and return the plugin's expanded location.
*/
function expandPlugin($file)
{
    printAndUnderline("Expanding " . basename($file));
    $dir = dirname($file);
    if (endsWith($file, ".obr")) {
        $cwd = getcwd();
        chdir($dir);
        runCommand("unzip " . $file);
        chdir($cwd);
    }
    return $dir;
}

/*
This function must
make sure the product has been moved into the TIS installation location.
*/
function moveProduct($productName, $expandedDir, $homeDir)
{
    printAndUnderline("Relocating " . $productName);
    $targetDir = join_paths($homeDir, "/inst/", strtolower($productName));
    if (file_exists($targetDir)) {
        exec("rm -r " . $targetDir);
    }
    runCommand("mv " . $expandedDir ." ". $targetDir);
}

/*
This function must
make sure the plugin has been moved into the product plugin location.
*/
function movePlugin($sourceDir, $targetDir)
{
    if (!file_exists($targetDir)) {
        if (!mkdir($targetDir, 0755, true)) {
            die('Cannot create plugin directory in ' . $targetDir);
        }
    }
    printAndUnderline("Relocating " . basename($sourceDir));
    $cwd = getcwd();
    chdir($sourceDir);
    runCommand("ls dependencies/*.jar 1> /dev/null 2>&1 && cp dependencies/*.jar " . $targetDir . " || echo No dependencies");
    runCommand("cp *.jar " . $targetDir);
    chdir($cwd);
}

function installProduct($product, $homeDir)
{
    echo("\n\n");
    //TODO: ideally, cache dirs are configurable from CLI or env vars.
    $downloadCacheDir = "/tmp/atlassian/tis/downloads/";
    $expandedCacheDir = "/tmp/atlassian/tis/expand/";
    $downloadedFile = downloadProduct($product['shortname'], $product['downloadurl'], $downloadCacheDir, $product['filename']);
    $expandedDir = expandProduct($product['shortname'], $downloadedFile, $expandedCacheDir, $product['filename'], $product['dirname']);
    moveProduct($product['shortname'], $expandedDir, $homeDir);

	//installing plugins
	printAndUnderline("Installing addons for " . $product['shortname']);
	if (isset($product['plugindir'])) {
		installPlugins($product['shortname'], $product['plugindir'], $product['plugins'], $homeDir, $downloadCacheDir);
	}
}

function installPlugins($productName, $pluginDir, $plugins, $homeDir, $downloadDir) {
	if ((is_array($plugins))) {
		foreach ($plugins as $plugin) {
            $name = end(explode('.', end(explode('/', $plugin))));
            $file = downloadPlugin($name, $plugin, $downloadDir, $productName);
            $sourceDir = expandPlugin($file);
            $targetDir = join_paths($homeDir, $pluginDir);
            movePlugin($sourceDir, $targetDir);
        }
    }
}

function runCommand($command) {
	echo("Running: " . $command . PHP_EOL);
	$proc = popen($command, "r");
    while (!feof($proc))
    {
        echo fread($proc, 4096);
        @ flush();
    }
    $rc = pclose($proc);
    if ($rc != 0) {
        echo("\n\nCommand failed: " . $command . PHP_EOL);
        //exit($rc);
    }
}

function printAndUnderline($instring) {
    echo(PHP_EOL);
    echo(str_repeat("-", 80) . PHP_EOL);
    echo("| ". $instring . str_repeat(" ", 77-strlen($instring)) . "|" . PHP_EOL);
    echo(str_repeat("-", 80) . PHP_EOL . PHP_EOL);
}

function join_paths() {
    $paths = array();
    foreach (func_get_args() as $arg) {
        if ($arg !== '') { $paths[] = $arg; }
    }
    return preg_replace('#/+#','/',join('/', $paths));
}

function endsWith($haystack, $needle)
{
    return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

//paramaterizes crowd.properties file for Bamboo, JIRA, and Confluence
//Bitbucket is in the home dir.  FeCru is in the DB
function connectCrowd($product, $homeDir) {

	//add crowd.properties to the right place in the install directory
	$config=file_get_contents($homeDir . "/scripts/crowd.properties");
	$config=str_replace("^applicationname^", strtolower($product['shortname']), $config);
	$config=str_replace("^applicationpassword^", strtolower($product['shortname']), $config);
	file_put_contents($homeDir . $product['crowdconfigfile'].'crowd.properties', $config);
	
	$config = '';
	
	//add seraph configuration for crowd/application
	$config=file_get_contents($homeDir . $product['crowdconfigfile'].'seraph-config.xml');
	$config=str_replace($product['crowdseraphold'], $product['crowdseraphnew'], $config);
	file_put_contents($homeDir . $product['crowdconfigfile'].'seraph-config.xml', $config);
	
}

function installCrowd($product, $homeDir) {
    
    installProduct($product, $homeDir);

    //Set the home directory
    $config = file_get_contents($homeDir . "/inst/crowd/crowd-webapp/WEB-INF/classes/crowd-init.properties");
    file_put_contents($homeDir . "/inst/crowd/crowd-webapp/WEB-INF/classes/crowd-init.properties", 
        "crowd.home=" . $homeDir. "/data/crowd\n" . $config);
    
    //set the default port from 8095 to 2430
    $config = file_get_contents($homeDir . "/inst/crowd/build.properties");
    file_put_contents($homeDir . "/inst/crowd/build.properties", 
    str_replace(8095, 2430, $config));
                      
    //run build.sh
    $installDir = getcwd();
    chdir($homeDir. "/inst/crowd/");
    runCommand("sh ./build.sh");
    chdir($installDir);
}

function installConfluence($product, $homeDir) {
    
    installProduct($product, $homeDir);
    $config = file_get_contents($homeDir . "/inst/confluence/confluence/WEB-INF/classes/confluence-init.properties");
    file_put_contents($homeDir . "/inst/confluence/confluence/WEB-INF/classes/confluence-init.properties", 
        "confluence.home=" . $homeDir. "/data/confluence\n" . $config);
    connectCrowd($product, $homeDir);

    runCommand("mkdir -p $homeDir/data/confluence/index/edge");
}

function installBamboo($product, $homeDir) {
    
    installProduct($product, $homeDir);
    $config = file_get_contents($homeDir . "/inst/bamboo/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties");
    file_put_contents($homeDir . "/inst/bamboo/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties", 
        "bamboo.home=" . $homeDir. "/data/bamboo\n" . $config);
    connectCrowd($product, $homeDir);
    
}

function installJIRA($product, $homeDir) {
    
    installProduct($product, $homeDir);
    connectCrowd($product, $homeDir);
    
}

function installBitbucket($product, $homeDir) {
    
    installProduct($product, $homeDir);
    
}

function installFeCru($product, $homeDir) {
    
    installProduct($product, $homeDir);
    chmod($homeDir. "/data/fecru/data/auth/developer-toolbox", 0400);
    
}

function installSourceTree($product, $homeDir) {
    runCommand("curl " . $product['downloadurl'] . " -o " . $product['filename']);
    runCommand("hdiutil mount " . $product['filename']);
    //if (file_exists($product['location'])) {
    //    exec("sudo rm -r " . $product['location']);
    //}   
    
    runCommand("sudo cp -R /Volumes/" . $product['dirname'] ." ". $product['location']);
    runCommand("rm " . $product['filename']); 
    runCommand("hdiutil unmount /Volumes/" . $product['dirname']);

}
?>

